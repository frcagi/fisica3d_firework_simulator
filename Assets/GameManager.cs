﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text enemyDestroy;
    public Text transcurredTime;
    public Text lifes;
    private string totalTime;
    private int enemyCounter =0;
    private float seconds, minutes;
    public BoatController mainCharacter;
    GameObject p1, p2;

    public int EnemyCounter { get => enemyCounter; set => enemyCounter = value; }
    public string TotalTime { get => totalTime; set => totalTime = value; }

    // Start is called before the first frame update
    void Start()
    {
       mainCharacter = GameObject.Find("Player").GetComponent<BoatController>();
    }

    // Update is called once per frame
    void Update()
    {
        minutes = (int)(Time.time / 60f);
        seconds = (int)(Time.time % 60f);
        TotalTime = minutes.ToString("00") + ": " + seconds.ToString("00");
        changeTexts();
    }

    void changeTexts() 
    { 
        transcurredTime.text = minutes.ToString("00") + ": " + seconds.ToString("00"); //Continuo cambio de tiempo transcurrido en UI
        enemyDestroy.text = "Enemy Ships Destroyed : " + enemyCounter; //Cambio de texto del numero de enemigos destruidos
        lifes.text = "Life: " + mainCharacter.health;
    }


}
