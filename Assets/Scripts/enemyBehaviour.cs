﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemyBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    public float moveSpeed = 5f;
    public Transform player;
    public Rigidbody rb;
    private Vector3 movement;
    public Text enemyShipsDestroyed;
    public float enemyCounter = 0;
    private GameManager gm;
    private void Awake()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        enemyShipsDestroyed = GameObject.FindGameObjectWithTag("textEnemy").GetComponent<Text>();
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        player = GameObject.Find("Player").transform;

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.ToLower().Equals("cannonball"))//Lo pasamos a minusculas TODO y lo comparamos a una tag
        {
            Destroy(gameObject); //Destruimos el barco
            Destroy(other.gameObject); //También destruimos la bala
            gm.EnemyCounter++; //Añadimos uno al contador
        }
    }

    private void FixedUpdate()
    {
        moveCharacter(movement);
    }
    private void Update()
    {
        Vector3 direction = player.position - transform.position;
        direction.Normalize();
        movement = direction;
    }
    void moveCharacter(Vector3 direction)
    {
        rb.MovePosition((Vector3)transform.position + (direction * moveSpeed * Time.deltaTime));
    }

}


