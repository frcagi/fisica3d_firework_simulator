﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BoatController : MonoBehaviour
{
    #region Variables
    public float seconds, minutes;
    public float health = 100f;
    private GameManager gm;
   

    #endregion  

    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>(); 
        //Buscamos el GM para pasar el tiempo una vez muertos
    }
    private void Update()
    {
        if (health <= 0)
        {
            PlayerPrefs.SetString(Constants.time, gm.TotalTime);                // Pasamos cuanto tiempo se ha sobrevivido para mostrarlo en la pantalla final
            PlayerPrefs.SetInt(Constants.enemiesDestroyed, gm.EnemyCounter);    // Pasamos cuantos enemigos se han destruído para mostrarlo en la pantalla final
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // Cambiamos a la escena GAMEOVER
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            health = health - 10; //Cada impacto resta 10 ptos de vida
            Destroy(collision.gameObject); //Destruimos el enemigo
        }
        
    }
}
