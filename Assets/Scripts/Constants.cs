﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    #region PlayerPrefs
    public static string time = "time";
    public static string enemiesDestroyed = "enemiesdestroyed";
    #endregion

}
