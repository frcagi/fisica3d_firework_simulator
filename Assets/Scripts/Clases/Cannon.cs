﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
   private float initialVelocity; //Velocidad inicial
   private float alphaAngle;  //Ángulo eje X
   private float gammaAngle;  //Ángulo eje Y
   private float size; //Tamaño del cañon
   private float lenght; //Altura del cañon

    public float InitialVelocity { get => initialVelocity; set => initialVelocity = value; }
    public float AlphaAngle { get => alphaAngle; set => alphaAngle = value; }
    public float GammaAngle { get => gammaAngle; set => gammaAngle = value; }
    public float Size { get => size; set => size = value; }
    public float Lenght { get => lenght; set => lenght = value; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
