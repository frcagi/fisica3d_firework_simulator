﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    public GameObject theEnemyPrefab;
    public GameObject enemyPrefab;
    public int xPos;
    public int zPos;

    void Start()
    {
        InvokeRepeating("EnemyDrop",1f, 4f); //Cada 4 segundos spawnean enemigos 
    }
    void EnemyDrop()
    {
       for(int i=0; i<5; i++)    {
            xPos = Random.Range(20, 100); //Sacamos posiciones aleatorias para el instanciamiento del enemigo
            zPos = Random.Range(20, 100); //Sacamos posiciones aleatorias para el instanciamiento del enemigo
            enemyPrefab = Instantiate(theEnemyPrefab, new Vector3(xPos, 0, zPos), Quaternion.identity) as GameObject;
       }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
