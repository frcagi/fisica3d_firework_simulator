using System.Collections.Generic;
using UnityEngine;

public class FireWorkController : MonoBehaviour
{
    public float initialVelocity;
    public float alphaAngle;  //Ángulo eje X
    public float gammaAngle;  //Ángulo eje Y
    public float size;
    public float lenght;
    public float gravity;
    private int numberOfFireWorkToDestroy;
    public GameObject fireworkPrefab;
    private GameObject canon;
    public List<GameObject> fireworksLaunched = new List<GameObject>();
    public bool autoPass;
    public GameObject firework = null;
    private Vector3 rotationCanon;
    float alpha, gamma;
    float incrementalTime;

    /*
     *  VIM (VERY IMPORTANT MESSAGE)
     *  NO TOCAR ABSOLUTAMENTE NADA DE ESTE SCRIPT
     */
    private void Awake()
    {
        canon = GameObject.Find("Cannon");
    }
    public int NumberOfFireWorkToDestroy { get => numberOfFireWorkToDestroy; 
                                           set => numberOfFireWorkToDestroy = value; 
                                         }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            /* 
             *  En el momento de disparo asignamos las variable del ángulo
             */
            alpha = alphaAngle;
            gamma = gammaAngle;
            createFireWork();
            autoPass = true;
        }
    }
    private void FixedUpdate()
    {
        if (autoPass)
        {
            for (int i = 0; i < fireworksLaunched.Count; i++)
            {
                if (fireworksLaunched[i] != null)
                {
                    Debug.Log(startBullet(lenght));
                    Vector vectorFirework = doSimulation(initialVelocity, firework.GetComponent<Shell>(), Time.deltaTime);
                    NumberOfFireWorkToDestroy = i;
                    fireworksLaunched[i].transform.position = vectorFirework.toVector3();
                }
            }
        }
    }
    #region Methods
    #region (cosThetas)
    /*
     * Cosenos directores calculados mediante las fórmulas de los componentes de la longitud y la proyección del cañon
     */
    float calculateThetaX(float alpha, float gamma)
    {
        float x = Mathf.Sin(gamma * Mathf.PI / 180.0f) * Mathf.Sin(alpha * Mathf.PI / 180.0f); 
       Debug.Log("theta x: " + x+"  alpha"+alpha+" gamma: "+gamma);
        return x;
    } 
    float calculateThetaY(float alpha)
    {

        float y = Mathf.Cos(alpha * Mathf.PI / 180.0f);
        Debug.Log("theta y: " + y + "  alpha" + alpha + " gamma: " + gamma);
        return y;
    }
    float calculateThetaZ(float alpha, float gamma)
    {
        float z = Mathf.Cos(gamma * Mathf.PI / 180.0f) * Mathf.Sin(alpha * Mathf.PI / 180.0f);
        Debug.Log("theta z: " + z + "  alpha" + alpha + " gamma: " + gamma);
        return z;
    }
    #endregion
   /*
    * 
    *  Calcula la posicion inicial de la bala a traves de los cosenos directores y la longitud del cañon 
    *  
    */
    Vector startBullet(float length)
    {
        return new Vector(length * calculateThetaX(alpha, gamma), length * calculateThetaY(alpha), length * (calculateThetaZ(alpha, gamma)));
    }
    #region firework
    /*
     *   Conjunto de fórmulas para el movimiento de la bala, en los tres ejes [(z,y,x) en Unity], obtenidos mediante los cosenos directores realizados anteriormente, 
     *   los ángulos ahora están introducidos por parametros en el inspector de Unity. 
     *   Por lo tanto obtendremos un resultado vectorial, que utilizaremos para determinar la trayectoria de la bala.
     *     
     */
    Vector doSimulation(float velocity, Shell firework, float time)
    {
        firework.incrementalTime += time;
        //float x = cannonPosition.xPos + velocity * calculateThetaX(alpha, gamma) * firework.incrementalTime;
        //float y = size + lenght * Mathf.Cos(alpha * Mathf.PI / 180.0f) + (velocity * calculateThetaY(alpha)) * firework.incrementalTime - (gravity * Mathf.Pow(firework.incrementalTime, 2) / 2);
        //float z = cannonPosition.zPos + velocity * calculateThetaZ(alpha, gamma) * firework.incrementalTime;

        // s =  v*t
        float x = 2.34f+velocity * calculateThetaX(alpha, gamma) * firework.incrementalTime;
        float y = 1.98f+(velocity * calculateThetaY(alpha)) * firework.incrementalTime - (gravity * Mathf.Pow(firework.incrementalTime, 2) / 2);
        float z = velocity * calculateThetaZ(alpha, gamma) * firework.incrementalTime;


        return new Vector(x, y, z);
    }
   /*
    * 
    *  GRAVEDAD = GRAVEDAD EN LA TIERRA
    *  INCREMENTAL TIME = INICIALMENTE A 0
    *  LO PASAMOS A PADRE (CANON) 
    *  Instanciamiento de la bala (prefab, posicion del canon, rotación a 0 (es una bala da igual la rotación))
    *  
    */
    void createFireWork()
    {
        if (firework == null)
        {
            firework = Instantiate(fireworkPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            firework.transform.parent = gameObject.transform;
            firework.GetComponent<Shell>().gravity = 9.8f; 
            firework.GetComponent<Shell>().incrementalTime = 0f;
            firework.transform.position = doSimulation(initialVelocity, firework.GetComponent<Shell>(), 0f).toVector3();
            fireworksLaunched.Add(firework);

        }
    }
    #endregion
    /*
     *  ESTO NO SE USA POR AHORA, ES PARA RECOGER EL ÁNGULO DEL CANNON 
     *  con .eulerAngles.X,.eulerAngles.Y,.eulerAngles.Z 
     */

        /*
         * Calcula la b y los elementos de la longitud
         */
    Vector calculateLongitudCannon(float length, float alpha, float gamma) {
        float b = length * Mathf.Sin(alpha * Mathf.PI/180.0f) ;
        float x = b * Mathf.Sin(gamma*Mathf.PI/180f);
        float y = length * Mathf.Cos(alpha * Mathf.PI / 180f);
        float z = b * Mathf.Cos(gamma * Mathf.PI / 180f);
        return new Vector(x,y,z);
    
    }
    Quaternion getRotationFromCanon()
    {
        return canon.transform.rotation;
    }
    #endregion

}