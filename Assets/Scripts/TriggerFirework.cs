﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerFirework : MonoBehaviour
{
    private FireWorkController fb;
    // Start is called before the first frame update
    void Start()
    {
        fb = gameObject.GetComponentInParent(typeof(FireWorkController)) as FireWorkController;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag.ToLower().Equals("cielo"))
        {
            GameObject fireworToDetroy = fb.fireworksLaunched[fb.NumberOfFireWorkToDestroy]; //buscamos la bala
            Destroy(gameObject); //La destruimos
            fb.fireworksLaunched.RemoveAt(fb.NumberOfFireWorkToDestroy); //La eliminamos del array que está en fireworkcontroller (Script de los cálculos y la simulación)
        }
    }
}
