﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralOcean : MonoBehaviour
{
    Mesh mesh; /*  Maya
                 *  Mesh Filter: Es la maya, contiene la info de la maya
                 *  Mesh Renderer: Renderizarla
                 *  CLASE CREADA GRACIAS A BRACKEYS 
                 *  VISITA SU CANAL EN YOUTUBE
                 */
    Vector3[] vertices; //Almacenaremos las posiciones de cada uno de los vertices de nuestra maya. 
    int[] triangulos; //Almacenaremos el numero del vertice (indice del array). 
    public int xMeshSize = 20;  //Se cambian estos valores
    public int zMeshSize = 20;  //Se cambian estos valores
    private Vector2 [] uvs; 
    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh(); //Inicializamos la maya.
        GetComponent<MeshFilter>().mesh = mesh; //La añadimos al componente mesh filter.
        createShape();
        updateMesh();
    }

    void createShape()
    {
        vertices = new Vector3[(xMeshSize + 1) * (zMeshSize + 1)];
        for (int i = 0, z = 0; z <= zMeshSize; z++)
        { //creamos la i también dentro del loop así solo se podrá usar en el loop    
            for (int x = 0; x <= xMeshSize; x++)
            {
                float y = Mathf.PerlinNoise(x * .3f,z*.3f)*1.2f; //Calculamos un valor de y, que servirá para la ola
                vertices[i] = new Vector3(x, y, z);
                i++;
            }
        }
        triangulos = new int[xMeshSize * zMeshSize * 6];// Ancho * alto * numero de vertices por cuadrado
        int vert = 0;
        int tris = 0;
        for (int z = 0; z < zMeshSize; z++)
        {
            for (int x = 0; x < xMeshSize; x++)
            {
                triangulos[tris + 0] = vert + 0;
                triangulos[tris + 1] = vert + xMeshSize + 1;
                triangulos[tris + 2] = vert + 1;
                triangulos[tris + 3] = vert + 1;
                triangulos[tris + 4] = vert + xMeshSize + 1;
                triangulos[tris + 5] = vert + xMeshSize + 2;
                vert++;
                tris += 6; //Para cada cuadrado necesitamos 6 vertices (tres por triangulo)
            }
            vert++;
        }
        uvs = new Vector2[vertices.Length];
        for (int i = 0, z = 0; z <= zMeshSize; z++)
        { //creamos la i también dentro del loop así solo se podrá usar en el loop    
            for (int x = 0; x <= xMeshSize; x++)
            {
                uvs[i] = new Vector2((float)x / xMeshSize, (float)z / zMeshSize);
                i++;
            }
        }


    }
    void updateMesh()
    {
        mesh.Clear();//La limpiamos
        mesh.vertices = vertices;
        mesh.triangles = triangulos;
        mesh.uv = uvs;
        mesh.RecalculateNormals();
    }

}
