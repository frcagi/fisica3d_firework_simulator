﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuFuncionalidad : MonoBehaviour
{

    public Text timer;
    public Text kills;
    string time;
    int enemies;

    // Start is called before the first frame update
    void Start()
    {
        time = PlayerPrefs.GetString(Constants.time);
        if (time == null||time.Trim().Equals("")) 
            time = "00:00";
        enemies = PlayerPrefs.GetInt(Constants.enemiesDestroyed);
        timer.text =  time;
        kills.text =  enemies.ToString("00");
       
    }

   
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
